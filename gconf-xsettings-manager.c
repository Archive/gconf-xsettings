/*
 * Copyright © 2001 Red Hat, Inc.
 *
 * Permission to use, copy, modify, distribute, and sell this software and its
 * documentation for any purpose is hereby granted without fee, provided that
 * the above copyright notice appear in all copies and that both that
 * copyright notice and this permission notice appear in supporting
 * documentation, and that the name of Red Hat not be used in advertising or
 * publicity pertaining to distribution of the software without specific,
 * written prior permission.  Red Hat makes no representations about the
 * suitability of this software for any purpose.  It is provided "as is"
 * without express or implied warranty.
 *
 * RED HAT DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING ALL
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL RED HAT
 * BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN 
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 * Authors:  Owen Taylor, Havoc Pennington
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <gtk/gtk.h>
#include <gdk/gdkx.h>

#include <gconf/gconf.h>

#include "xsettings-manager.h"



#define G_N_ELEMENTS(arr)		(sizeof (arr) / sizeof ((arr)[0]))

static XSettingsManager *manager;

static const char* listen_directories[] = {
  "/desktop/standard",
  "/desktop/gnome",
  "/desktop/gtk"
};

typedef struct _TranslationEntry TranslationEntry;

typedef void (* TranslationFunc) (TranslationEntry *trans,
                                  GConfValue       *value);

struct _TranslationEntry
{
  const char *gconf_key;
  const char *xsetting_name;
  
  GConfValueType gconf_type;

  TranslationFunc translate;
};

static void translate_int_int       (TranslationEntry *trans,
                                     GConfValue       *value);
static void translate_string_string (TranslationEntry *trans,
                                     GConfValue       *value);

static TranslationEntry translations [] = {
  { "/desktop/standard/double-click-time", "Net/DoubleClickTime", GCONF_VALUE_INT,    
    translate_int_int },
  { "/desktop/gtk/gtk-color-palette", "Gtk/ColorPalette", GCONF_VALUE_STRING,
    translate_string_string },
  { "/desktop/gtk/gtk-toolbar-style", "Gtk/ToolbarStyle", GCONF_VALUE_STRING,
    translate_string_string },
  { "/desktop/gtk/gtk-toolbar-icon-size", "Gtk/ToolbarIconSize", GCONF_VALUE_STRING,
    translate_string_string }
};

static TranslationEntry*
find_translation_entry (const char *gconf_key)
{
  int i;

  i = 0;
  while (i < G_N_ELEMENTS (translations))
    {
      if (strcmp (translations[i].gconf_key, gconf_key) == 0)
        return &translations[i];

      ++i;
    }

  return NULL;
}

static const gchar* 
type_to_string (GConfValueType type)
{
  switch (type)
    {
    case GCONF_VALUE_INT:
      return "int";
      break;
    case GCONF_VALUE_STRING:
      return "string";
      break;
    case GCONF_VALUE_FLOAT:
      return "float";
      break;
    case GCONF_VALUE_BOOL:
      return "bool";
      break;
    case GCONF_VALUE_SCHEMA:
      return "schema";
      break;
    case GCONF_VALUE_LIST:
      return "list";
      break;
    case GCONF_VALUE_PAIR:
      return "pair";
      break;
    case GCONF_VALUE_INVALID:
      return "*invalid*";
      break;
    default:
      g_assert_not_reached();
      return NULL; /* for warnings */
      break;
    }
}

static void
process_value (TranslationEntry *trans,
               GConfValue       *val)
{  
  if (val == NULL)
    {
      xsettings_manager_delete_setting (manager, trans->xsetting_name);
    }
  else
    {
      if (val->type == trans->gconf_type)
        {
          (* trans->translate) (trans, val);
        }
      else
        {
          g_warning ("GConf key %s set to type %s but its expected type was %s\n",
                     trans->gconf_key,
                     type_to_string (val->type),
                     type_to_string (trans->gconf_type));
        }
    }
}

static void
config_notify (GConfEngine *client,
               guint        cnxn_id,
               GConfEntry  *entry,
               gpointer     user_data)
{
  TranslationEntry *trans;

  trans = find_translation_entry (entry->key);

  if (trans == NULL)
    return;

  process_value (trans, entry->value);
  
  xsettings_manager_notify (manager);
}

static void
translate_int_int (TranslationEntry *trans,
                   GConfValue       *value)
{
  g_assert (value->type == trans->gconf_type);
  
  xsettings_manager_set_int (manager, trans->xsetting_name,
                             gconf_value_get_int (value));
}

static void
translate_string_string (TranslationEntry *trans,
                         GConfValue       *value)
{
  g_assert (value->type == trans->gconf_type);

  xsettings_manager_set_string (manager,
                                trans->xsetting_name,
                                gconf_value_get_string (value));
}

static void
load_all (GConfEngine *engine)
{
  int i;

  i = 0;
  while (i < G_N_ELEMENTS (translations))
    {
      GConfValue *val;
      GError *err;

      err = NULL;
      val = gconf_engine_get (engine,
                              translations[i].gconf_key,
                              &err);

      if (err != NULL)
        {
          fprintf (stderr, "Error getting value for %s: %s\n",
                   translations[i].gconf_key, err->message);
          g_error_free (err);
        }
      else
        {
          process_value (&translations[i], val);
        }
      
      ++i;
    }

  xsettings_manager_notify (manager);
}

void
terminate_cb (void *data)
{
  gboolean *terminated = data;
  
  *terminated = TRUE;
  gtk_main_quit ();
}

static GdkFilterReturn 
manager_event_filter (GdkXEvent *xevent,
		      GdkEvent  *event,
		      gpointer   data)
{
  if (xsettings_manager_process_event (manager, (XEvent *)xevent))
    return GDK_FILTER_REMOVE;
  else
    return GDK_FILTER_CONTINUE;
}

int 
main (int argc, char **argv)
{
  gboolean terminated = FALSE;
  GConfEngine *engine;
  int i;
  GError *err;
  
  gtk_init (&argc, &argv);  
  
  if (xsettings_manager_check_running (gdk_display, DefaultScreen (gdk_display)))
    {
      fprintf (stderr, "You can only run one xsettings manager at a time; exiting");
      exit (1);
    }
      
  if (!terminated)
    {
      manager = xsettings_manager_new (gdk_display, DefaultScreen (gdk_display),
				       terminate_cb, &terminated);
      if (!manager)
	{
	  fprintf (stderr, "Could not create xsettings manager!");
	  exit (1);
	}
    }

  gconf_init (argc, argv, NULL); /* exits w/ message on failure */  

  /* We use GConfEngine not GConfClient because a cache isn't useful
   * for us
   */
  engine = gconf_engine_get_default ();

  i = 0;
  while (i < G_N_ELEMENTS (listen_directories))
    {
      err = NULL;
      gconf_engine_notify_add (engine,
                               listen_directories[i],
                               config_notify,
                               NULL,
                               &err);

      if (err)
        {
          fprintf (stderr, "Could not listen for changes to configuration in '%s': %s\n",
                   listen_directories[i], err->message);
          g_error_free (err);
        }
      
      ++i;
    }
  
  gdk_window_add_filter (NULL, manager_event_filter, NULL);

  load_all (engine);
  
  if (!terminated)
    gtk_main ();
  
  xsettings_manager_destroy (manager);
  gconf_engine_unref (engine);

  g_print ("gconf-xsettings-manager exiting\n");
  
  return 0;
}
